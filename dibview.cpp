// dibview.cpp : implementation of the CDibView class
//
// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) 1992-1998 Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

#include "stdafx.h"
#include "diblook.h"

#include "dibdoc.h"
#include "dibview.h"
#include "dibapi.h"
#include "mainfrm.h"
#include <queue>
#include <cstddef>

#include "HRTimer.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif



#define BEGIN_PROCESSING() INCEPUT_PRELUCRARI()

#define END_PROCESSING(Title) SFARSIT_PRELUCRARI(Title)

#define INCEPUT_PRELUCRARI() \
	CDibDoc* pDocSrc=GetDocument();										\
	CDocTemplate* pDocTemplate=pDocSrc->GetDocTemplate();				\
	CDibDoc* pDocDest=(CDibDoc*) pDocTemplate->CreateNewDocument();		\
	BeginWaitCursor();													\
	HDIB hBmpSrc=pDocSrc->GetHDIB();									\
	HDIB hBmpDest = (HDIB)::CopyHandle((HGLOBAL)hBmpSrc);				\
	if ( hBmpDest==0 ) {												\
		pDocTemplate->RemoveDocument(pDocDest);							\
		return;															\
	}																	\
	BYTE* lpD = (BYTE*)::GlobalLock((HGLOBAL)hBmpDest);					\
	BYTE* lpS = (BYTE*)::GlobalLock((HGLOBAL)hBmpSrc);					\
	int iColors = DIBNumColors((char *)&(((LPBITMAPINFO)lpD)->bmiHeader)); \
	RGBQUAD *bmiColorsDst = ((LPBITMAPINFO)lpD)->bmiColors;	\
	RGBQUAD *bmiColorsSrc = ((LPBITMAPINFO)lpS)->bmiColors;	\
	BYTE * lpDst = (BYTE*)::FindDIBBits((LPSTR)lpD);	\
	BYTE * lpSrc = (BYTE*)::FindDIBBits((LPSTR)lpS);	\
	int dwWidth  = ::DIBWidth((LPSTR)lpS);\
	int dwHeight = ::DIBHeight((LPSTR)lpS);\
	int w=WIDTHBYTES(dwWidth*((LPBITMAPINFOHEADER)lpS)->biBitCount);	\
	HRTimer my_timer;	\
	my_timer.StartTimer();	\

#define BEGIN_SOURCE_PROCESSING \
	CDibDoc* pDocSrc=GetDocument();										\
	BeginWaitCursor();													\
	HDIB hBmpSrc=pDocSrc->GetHDIB();									\
	BYTE* lpS = (BYTE*)::GlobalLock((HGLOBAL)hBmpSrc);					\
	int iColors = DIBNumColors((char *)&(((LPBITMAPINFO)lpS)->bmiHeader)); \
	RGBQUAD *bmiColorsSrc = ((LPBITMAPINFO)lpS)->bmiColors;	\
	BYTE * lpSrc = (BYTE*)::FindDIBBits((LPSTR)lpS);	\
	int dwWidth  = ::DIBWidth((LPSTR)lpS);\
	int dwHeight = ::DIBHeight((LPSTR)lpS);\
	int w=WIDTHBYTES(dwWidth*((LPBITMAPINFOHEADER)lpS)->biBitCount);	\
	


#define END_SOURCE_PROCESSING	\
	::GlobalUnlock((HGLOBAL)hBmpSrc);								\
    EndWaitCursor();												\
/////////////////////////////////////////////////////////////////////////////


//---------------------------------------------------------------
#define SFARSIT_PRELUCRARI(Titlu)	\
	double elapsed_time_ms = my_timer.StopTimer();	\
	CString Title;	\
	Title.Format(_TEXT("%s - Proc. time = %.2f ms"), _TEXT(Titlu), elapsed_time_ms);	\
	::GlobalUnlock((HGLOBAL)hBmpDest);								\
	::GlobalUnlock((HGLOBAL)hBmpSrc);								\
    EndWaitCursor();												\
	pDocDest->SetHDIB(hBmpDest);									\
	pDocDest->InitDIBData();										\
	pDocDest->SetTitle((LPCTSTR)Title);									\
	CFrameWnd* pFrame=pDocTemplate->CreateNewFrame(pDocDest,NULL);	\
	pDocTemplate->InitialUpdateFrame(pFrame,pDocDest);	\

/////////////////////////////////////////////////////////////////////////////
// CDibView

IMPLEMENT_DYNCREATE(CDibView, CScrollView)

BEGIN_MESSAGE_MAP(CDibView, CScrollView)
	//{{AFX_MSG_MAP(CDibView)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateEditPaste)
	ON_MESSAGE(WM_DOREALIZE, OnDoRealize)
	ON_COMMAND(ID_PROCESSING_PARCURGERESIMPLA, OnProcessingParcurgereSimpla)
	//}}AFX_MSG_MAP

	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CScrollView::OnFilePrintPreview)
	ON_COMMAND(ID_PROIECT_BINARIZARE, &CDibView::OnProiectBinarizare)
	ON_COMMAND(ID_PROIECT_OPERATIIMORFOLOGICE, &CDibView::OnProiectOperatiimorfologice)
	ON_COMMAND(ID_PROIECT_ETICHETARE, &CDibView::OnProiectEtichetare)
	ON_COMMAND(ID_PROIECT_PERIMETRUCENTRU, &CDibView::OnProiectPerimetrucentru)
	ON_COMMAND(ID_PROIECT_CALCULVARFURI, &CDibView::OnProiectCalculvarfuri)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDibView construction/destruction

CDibView::CDibView()
{
}

CDibView::~CDibView()
{
}

/////////////////////////////////////////////////////////////////////////////
// CDibView drawing

void CDibView::OnDraw(CDC* pDC)
{
	CDibDoc* pDoc = GetDocument();

	HDIB hDIB = pDoc->GetHDIB();
	if (hDIB != NULL)
	{
		LPSTR lpDIB = (LPSTR) ::GlobalLock((HGLOBAL) hDIB);
		int cxDIB = (int) ::DIBWidth(lpDIB);         // Size of DIB - x
		int cyDIB = (int) ::DIBHeight(lpDIB);        // Size of DIB - y
		::GlobalUnlock((HGLOBAL) hDIB);
		CRect rcDIB;
		rcDIB.top = rcDIB.left = 0;
		rcDIB.right = cxDIB;
		rcDIB.bottom = cyDIB;
		CRect rcDest;
		if (pDC->IsPrinting())   // printer DC
		{
			// get size of printer page (in pixels)
			int cxPage = pDC->GetDeviceCaps(HORZRES);
			int cyPage = pDC->GetDeviceCaps(VERTRES);
			// get printer pixels per inch
			int cxInch = pDC->GetDeviceCaps(LOGPIXELSX);
			int cyInch = pDC->GetDeviceCaps(LOGPIXELSY);

			//
			// Best Fit case -- create a rectangle which preserves
			// the DIB's aspect ratio, and fills the page horizontally.
			//
			// The formula in the "->bottom" field below calculates the Y
			// position of the printed bitmap, based on the size of the
			// bitmap, the width of the page, and the relative size of
			// a printed pixel (cyInch / cxInch).
			//
			rcDest.top = rcDest.left = 0;
			rcDest.bottom = (int)(((double)cyDIB * cxPage * cyInch)
					/ ((double)cxDIB * cxInch));
			rcDest.right = cxPage;
		}
		else   // not printer DC
		{
			rcDest = rcDIB;
		}
		::PaintDIB(pDC->m_hDC, &rcDest, pDoc->GetHDIB(),
			&rcDIB, pDoc->GetDocPalette());
	}
}

/////////////////////////////////////////////////////////////////////////////
// CDibView printing

BOOL CDibView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

/////////////////////////////////////////////////////////////////////////////
// CDibView commands


LRESULT CDibView::OnDoRealize(WPARAM wParam, LPARAM)
{
	ASSERT(wParam != NULL);
	CDibDoc* pDoc = GetDocument();
	if (pDoc->GetHDIB() == NULL)
		return 0L;  // must be a new document

	CPalette* pPal = pDoc->GetDocPalette();
	if (pPal != NULL)
	{
		CMainFrame* pAppFrame = (CMainFrame*) AfxGetApp()->m_pMainWnd;
		ASSERT_KINDOF(CMainFrame, pAppFrame);

		CClientDC appDC(pAppFrame);
		// All views but one should be a background palette.
		// wParam contains a handle to the active view, so the SelectPalette
		// bForceBackground flag is FALSE only if wParam == m_hWnd (this view)
		CPalette* oldPalette = appDC.SelectPalette(pPal, ((HWND)wParam) != m_hWnd);

		if (oldPalette != NULL)
		{
			UINT nColorsChanged = appDC.RealizePalette();
			if (nColorsChanged > 0)
				pDoc->UpdateAllViews(NULL);
			appDC.SelectPalette(oldPalette, TRUE);
		}
		else
		{
			TRACE0("\tSelectPalette failed in CDibView::OnPaletteChanged\n");
		}
	}

	return 0L;
}

void CDibView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();
	ASSERT(GetDocument() != NULL);

	SetScrollSizes(MM_TEXT, GetDocument()->GetDocSize());
}


void CDibView::OnActivateView(BOOL bActivate, CView* pActivateView,
					CView* pDeactiveView)
{
	CScrollView::OnActivateView(bActivate, pActivateView, pDeactiveView);

	if (bActivate)
	{
		ASSERT(pActivateView == this);
		OnDoRealize((WPARAM)m_hWnd, 0);   // same as SendMessage(WM_DOREALIZE);
	}
}

void CDibView::OnEditCopy()
{
	CDibDoc* pDoc = GetDocument();
	// Clean clipboard of contents, and copy the DIB.

	if (OpenClipboard())
	{
		BeginWaitCursor();
		EmptyClipboard();
		SetClipboardData (CF_DIB, CopyHandle((HANDLE) pDoc->GetHDIB()) );
		CloseClipboard();
		EndWaitCursor();
	}
}



void CDibView::OnUpdateEditCopy(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(GetDocument()->GetHDIB() != NULL);
}


void CDibView::OnEditPaste()
{
	HDIB hNewDIB = NULL;

	if (OpenClipboard())
	{
		BeginWaitCursor();

		hNewDIB = (HDIB) CopyHandle(::GetClipboardData(CF_DIB));

		CloseClipboard();

		if (hNewDIB != NULL)
		{
			CDibDoc* pDoc = GetDocument();
			pDoc->ReplaceHDIB(hNewDIB); // and free the old DIB
			pDoc->InitDIBData();    // set up new size & palette
			pDoc->SetModifiedFlag(TRUE);

			SetScrollSizes(MM_TEXT, pDoc->GetDocSize());
			OnDoRealize((WPARAM)m_hWnd,0);  // realize the new palette
			pDoc->UpdateAllViews(NULL);
		}
		EndWaitCursor();
	}
}


void CDibView::OnUpdateEditPaste(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(::IsClipboardFormatAvailable(CF_DIB));
}

void CDibView::OnProcessingParcurgereSimpla() 
{
	// TODO: Add your command handler code here
	BEGIN_PROCESSING();

	// Makes a grayscale image by equalizing the R, G, B components from the LUT
	for (int k=0;  k < iColors ; k++)
		bmiColorsDst[k].rgbRed=bmiColorsDst[k].rgbGreen=bmiColorsDst[k].rgbBlue=k;

	//  Goes through the bitmap pixels and performs their negative	
	for (int i=0;i<dwHeight;i++)
		for (int j=0;j<dwWidth;j++)
		  {	
			lpDst[i*w+j]= 255 - lpSrc[i*w+j]; //makes image negative
	  }

	END_PROCESSING("Negativ imagine");
}


int MIN(int a, int b, int c){
	if (a < b){
		if (a < c){
			return a;
		}
		else{
			return c;
		}
	}
	else{
		if (b < c){
			return b;
		}
		else{
			return c;
		}

	}
}

int MAX(int a, int b, int c){
	if (a > b){
		if (a > c){
			return a;
		}
		else{
			return c;
		}
	}
	else{
		if (b > c){
			return b;
		}
		else{
			return c;
		}

	}
}

void RGBtoHSV(float r, float g, float b, float *h, float *s, float *v)
{
	float min, max, delta;
	min = MIN(r, g, b);
	max = MAX(r, g, b);
	*v = max;				// v
	delta = max - min;
	if (max != 0)
		*s = delta / max;		// s
	else {
		// r = g = b = 0		// s = 0, v is undefined
		*s = 0;
		*h = -1;
		return;
	}
	if (r == max)
		*h = (g - b) / delta;		// between yellow & magenta
	else if (g == max)
		*h = 2 + (b - r) / delta;	// between cyan & yellow
	else
		*h = 4 + (r - g) / delta;	// between magenta & cyan
	*h *= 60;				// degrees
	if (*h < 0)
		*h += 360;
}

void dilatare(byte* temp, byte* lpDst, int dwWidth, int dwHeight, int kernel){
	
	for (int i = 1; i<dwHeight - 1; i++)
		for (int j = 1; j < dwWidth - 1; j++){
			if (temp[i * dwWidth + j] == 0){
				for (int k = -kernel / 2; k < kernel / 2; k++){
					for (int l = -kernel / 2; l < kernel / 2; l++){
						if (i + k > 0 && i + k < dwHeight && j + l>0 && j + l < dwWidth){
							lpDst[(i + k)*dwWidth +  (j + l)] = 0;
						}
					}
				}
			}
		}
}

void eroziune(byte* temp, byte* lpDst, int dwWidth, int dwHeight, int kernel){

	bool cond = false;

	for (int i = 1; i<dwHeight - 1; i++)
		for (int j = 1; j < dwWidth - 1; j++){
			if (temp[i * dwWidth + j] == 0){
				for (int k = -kernel / 2; k < kernel / 2; k++){
					for (int l = -kernel / 2; l < kernel / 2; l++){
						if ((i + k) > 0 && (i + k) < dwHeight && (j + l)>0 && (j + l) < dwWidth){
							if (temp[(i + k)*dwWidth + (j + l)] == 255){
								cond = true;
							}
						}
					}
				}
				if (cond){
					lpDst[i*dwWidth + j] = 255;
				}
				cond = false;
			}
		}
}

BYTE *initValues;
void CDibView::OnProiectBinarizare()
{
	BEGIN_PROCESSING();
	initValues = new BYTE[w*dwHeight*3];

	float h, s, v, r, g, b;

	if (iColors > 0){
		for (int i = 0; i < dwHeight; i++){
			printf("\n");
			for (int j = 0; j < dwWidth; j++){
				r = bmiColorsSrc[lpSrc[i*w + j]].rgbRed;
				g = bmiColorsSrc[lpSrc[i*w + j]].rgbGreen;
				b = bmiColorsSrc[lpSrc[i*w + j]].rgbBlue;
				printf("%d %d %d | ", b, g, r);
				initValues[i*w + 3 * j] = b;
				initValues[i*w + 3 * j + 1] = g;
				initValues[i*w + 3 * j + 2] = r;

				RGBtoHSV(r, g, b, &h, &s, &v);
				if (((h <= 30 && h >= 0) || (h >= 330 && h <= 360)) && (s>0.7)){
					lpDst[i*w + j] = 0;

				}
				else if (((h <= 270 && h >= 210)) && (s>0.5)){
					lpDst[i*w + j] = 0;

				}
				else{
					lpDst[i*w + j] = 255;

				}
			}
		}
	}
	END_PROCESSING("Binarizare");
}


void CDibView::OnProiectOperatiimorfologice()
{
	BEGIN_PROCESSING();
	int kernel = 4;
	BYTE *temp = new BYTE[w*dwHeight];
	memcpy(temp, lpSrc, w*dwHeight);
	eroziune(temp, lpDst, w, dwHeight, kernel);

	memcpy(temp, lpDst, w*dwHeight);
	dilatare(temp, lpDst, w, dwHeight, kernel + 2);
	
	END_PROCESSING("Operatii morfologice");
}


typedef struct pair{
	int a;
	int b;
}NewPair;

std::queue<int> q;

void CDibView::OnProiectEtichetare()
{
	BEGIN_PROCESSING();
	// TODO: Add your command handler code here
	NewPair perechi[1000];
	int nr_perechi = 0;
	int *matEt = new int[w*dwHeight];
	int claseEchiv[600] = { 0 };
	int newlabel = 0;
	for (int i = dwHeight - 2; i >= 0; i--)
		for (int j = 0; j < dwWidth - 1; j++){
			int X = lpSrc[i*w + j];
			int A = lpSrc[i*w + j - 1];
			int B = lpSrc[(i + 1)*w + j - 1];
			int C = lpSrc[(i + 1)*w + j];
			int D = lpSrc[(i + 1)*w + j + 1];
			int &X_label = matEt[i*w + j];
			int &A_label = matEt[i*w + j - 1];
			int &B_label = matEt[(i + 1)*w + j - 1];
			int &C_label = matEt[(i + 1)*w + j];
			int &D_label = matEt[(i + 1)*w + j + 1];
			if (X == 0)
				if (B == 0){
					if (C != 0 && D == 0){
						X_label = B_label;
						//NewPair(B_Label,D_label)
						perechi[nr_perechi].a = B_label;
						perechi[nr_perechi].b = D_label;
						nr_perechi++;
					}
					else
					{
						X_label = B_label;
					}
				}
				else
				{
					if (A == 0)
						if (C == 0){
							X_label = A_label;
							//NewPair(A_Label,C_label)
							perechi[nr_perechi].a = A_label;
							perechi[nr_perechi].b = C_label;
							nr_perechi++;
						}
						else
						{
							if (D == 0){
								X_label = A_label;
								//NewPair(A_Label,D_label)
								perechi[nr_perechi].a = A_label;
								perechi[nr_perechi].b = D_label;
								nr_perechi++;
							}
							else
							{
								X_label = A_label;
							}
						}
					else
					{
						if (C == 0){
							X_label = C_label;
						}
						else
						{
							if (D == 0)
								X_label = D_label;
							else
							{
								newlabel++;
								X_label = newlabel;
							}
						}
					}
				}


		}
	//2
	int nrClEchiv = 0;
	for (int i = 1; i <= newlabel; i++){
		if (claseEchiv[i] == 0){
			q.push(i);
			nrClEchiv++;
			claseEchiv[i] = nrClEchiv;
			while (!q.empty()){
				int n = q.front();
				q.pop();
				for (int j = 0; j <nr_perechi; j++){
					if (perechi[j].a == n){
						if (claseEchiv[perechi[j].b] == 0){
							q.push(perechi[j].b);
							claseEchiv[perechi[j].b] = nrClEchiv;
						}
					}
					if (perechi[j].b == n){
						if (claseEchiv[perechi[j].a] == 0){
							q.push(perechi[j].a);
							claseEchiv[perechi[j].a] = nrClEchiv;
						}
					}
				}
			}
		}
	}

	srand(time(NULL));
	for (int i = 0; i<dwHeight - 2; i++)
		for (int j = 0; j < dwWidth - 1; j++){
			if (lpSrc[i*w + j] == 0){
				lpDst[i*w + j] = claseEchiv[matEt[i*w + j]];
			}
		}


	for (int k = 1; k <= 254; k++){
		bmiColorsDst[k].rgbRed = rand() % 256;
		bmiColorsDst[k].rgbGreen = rand() % 256;
		bmiColorsDst[k].rgbBlue = rand() % 256;
	}

	delete[]matEt;
	END_PROCESSING("Etichetare");
}

typedef struct perimeter{
	int color;
	int points;
	int xcoord;
	int ycoord;
	perimeter *next;
}perim;

perim *perimeters = NULL;

void add(int color,int xcoord,int ycoord){
	if (perimeters == NULL){
		perimeters = (perim*)malloc(sizeof(perim));
		perimeters->color= color;
		perimeters->points = 1;
		perimeters->xcoord = xcoord;
		perimeters->ycoord = ycoord;
		perimeters->next = NULL;
	}
	else{
		perim *interm;
		interm = perimeters;
		while (interm->next != NULL && interm->color != color){
			interm = interm->next;
		}
		if (interm->color == color){
			interm->points++;
			interm->xcoord += xcoord;
			interm->ycoord += ycoord;
		}
		else{
			perim *interm2;
			interm2 = (perim*)malloc(sizeof(perim));
			interm2->color = color;
			interm2->points = 1;
			interm2->xcoord = xcoord;
			interm2->ycoord = ycoord;
			interm2->next = NULL;
			interm->next = interm2;
			
		}
	}
}
	
	
void CDibView::OnProiectPerimetrucentru()
{
	BEGIN_PROCESSING();
	int sum;
	int d[3][3] = { 1, 1, 1, 1, -8, 1, 1, 1, 1 };
	for (int i = 1; i < dwHeight-1; i++)
		for (int j = 1; j < dwWidth-1; j++){
			if (lpSrc[i*w + j] != 255){
					add(lpSrc[i*w + j], j, i);
				}
		}

	perim *interm = perimeters;
	while (interm != NULL){
		int x = interm->xcoord / interm->points;
		int y = interm->ycoord / interm->points;
		if (interm->points > 100){
			for (int i = 0; i < 6; i++)
				for (int j = 0; j < 6; j++){
					lpDst[(i + y - 3)*w + j + x - 3] = 0;
				}
		}

		interm = interm->next;
	}
	END_PROCESSING("CalculPerimetru");
}

typedef struct point{
	int x;
	int y;
	point *next;
};

typedef struct node{
	int color;
	int centerx;
	int centery;
	int perimeter;
	int max;
	int preak;
	node *next;
	point *points;

};

node *objects = NULL;

void addObject(int color,int centerx,int centery,int perimeter){
	

	if (objects == NULL){
		objects = (node*)malloc(sizeof(node));
		objects->color = color;
		objects->centerx = centerx;
		objects->centery = centery;
		objects->perimeter = perimeter;
		objects->preak = 0;
		objects->max = 0;
		objects->next = NULL;
		objects->points = NULL;
	}
	else{
		node *interm = objects;
		node *interm1 = (node*)malloc(sizeof(node));
		interm1->color = color;
		interm1->centerx = centerx;
		interm1->centery = centery;
		interm1->preak = 0;
		interm1->perimeter = perimeter;
		interm1->max = 0;
		interm1->next = NULL;
		interm1->points = NULL;
		while (interm->next != NULL){
			interm = interm->next;
		}
		interm->next = interm1;
	}
}

bool pushpoints(int x, int y, point **points,int newSet){
	int th = 23;
	int sf = true;

	if (newSet == 1){
		*points = (point*)malloc(sizeof(point));
		(*points)->next = NULL;
		(*points)->x = x;
		(*points)->y = y;
	}
	else{
		if (*points == NULL){
			*points = (point*)malloc(sizeof(point));
			(*points)->next = NULL;
			(*points)->x = x;
			(*points)->y = y;
		}
		else{
			point *interm;
			interm = *points;
			while (interm->next != NULL && sf){
				int distance = sqrt((interm->x - x)*(interm->x - x) + (interm->y - y)*(interm->y - y));
				if (distance < th){
					sf = false;
				}
				interm = interm->next;
			}
			int distance = sqrt((interm->x - x)*(interm->x - x) + (interm->y - y)*(interm->y - y));
			if (distance < th){
				sf = false;
			}

			if (sf){
				point *interm1;
				interm1 = (point*)malloc(sizeof(point));
				interm1->next = NULL;
				interm1->x = x;
				interm1->y = y;
				interm->next = interm1;
			}
		}
	}
	return sf;
}

void addpoint(int x, int y, int color){
	int th = 7;
	node *interm = objects;
	while (interm != NULL){
		if (interm->color == color){
			int distance = sqrt((interm->centerx - x)*(interm->centerx - x) + (interm->centery - y)*(interm->centery - y));
			if (interm->max + th < distance){
				interm->max = distance;
				interm->preak = 1;
				pushpoints(x, y, &(interm->points),1);
			}
			else{
				if (distance + th -6 > interm->max)
					if (pushpoints(x, y, &(interm->points), 0)){
						interm->preak++;
					}
			}
		}
		interm = interm->next;
	}
}

void CDibView::OnProiectCalculvarfuri()
{
	BEGIN_PROCESSING();
	int sum;
	int d[3][3] = { 1, 1, 1, 1, -8, 1, 1, 1, 1 };
	perim *interm,*interm1;
	interm = perimeters;
		while (interm != NULL){
			if (interm->points > 200){
				int x = interm->xcoord / interm->points;
				int y = interm->ycoord / interm->points;
				addObject(interm->color, x, y, interm->points);
			}
			interm1 = interm;
			interm = interm->next;
			free(interm1);
		}
		for (int i = 0; i < dwHeight; i++)
			for (int j = 0; j < dwWidth; j++){
				sum = 0;
				if (lpSrc[i*w + j] != 255){
					for (int k = 0; k < 3; k++)
						for (int l = 0; l < 3; l++){
							sum = sum + d[k][l] * lpSrc[(i + k - 1)*w + j + l - 1];
						}
				}
				if (sum > 0){
					addpoint(j, i, lpSrc[i*w + j]);
				}
			}

		node *interms1,*interms = objects;
		point *interms2;

		while (interms!= NULL){

			float h, s, v, r, g, b;
			r = initValues[interms->points->y*w + 3 * interms->points->x + 2];
			g = initValues[interms->points->y*w + 3 * interms->points->x + 1];
			b = initValues[interms->points->y*w + 3 * interms->points->x ];

			RGBtoHSV(r, g, b, &h, &s, &v);
			//if (((h <= 30 && h >= 0) || (h >= 330 && h <= 360)) && (s>0.7)){
				
				if (interms->preak == 3){
					CString info;
					info.Format(_TEXT("Semn Atentie \n Culoare Rosu \n Nr Varfuri=%d\n Coord centru=(%d,%d) "), interms->preak, interms->centerx, interms->centery);
					AfxMessageBox(info);
				}else if (interms->preak == 8){
					CString info;
					info.Format(_TEXT("Semn Stop \n Culoare Rosu \n Nr Varfuri=%d\n Coord centru=(%d,%d) "), interms->preak, interms->centerx, interms->centery);
					AfxMessageBox(info);
				}
				else if(interms->preak == 4){
					CString info;
					info.Format(_TEXT("Semn informativ \n Culoare Albastru \n Nr Varfuri=%d\n Coord centru=(%d,%d) "), interms->preak, interms->centerx, interms->centery);
					AfxMessageBox(info);
				}else
					if (interms->preak > 8){
					CString info;
					info.Format(_TEXT("Semn Interzis \n Culoare Rosu \n Nr Varfuri=%d\n Coord centru=(%d,%d) "), interms->preak, interms->centerx, interms->centery);
					AfxMessageBox(info);
				}
				
			//}else if (((h <= 270 && h >= 210)) && (s>0.5)){
			//	if (interms->preak > 8){
				//	CString info;
			//		info.Format(_TEXT("Semn Obligatoriu \n Culoare Albastru \n Nr Varfuri=%d\n Coord centru=(%d,%d) "), interms->preak, interms->centerx, interms->centery);
			//		AfxMessageBox(info);
			//	}

			//}

			while (interms->points != NULL){
				for (int i = 0; i < 6; i++)
					for (int j = 0; j < 6; j++){
						lpDst[(i + interms->points->y - 3)*w + j + interms->points->x - 3] = 0;
					}
				interms2 = interms->points;
				interms->points = interms->points->next;
				free(interms2);
			}

			interms1 = interms;
			interms = interms->next;
			free(interms1);
		}
		
	END_PROCESSING("CalculVarfuri");
}
	
